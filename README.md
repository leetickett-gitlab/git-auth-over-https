Gitlab auth over https from Git command line
============================================

This repo provides an environment sufficent to
verify proper behavior of new authentication routes
added to Gitlab via [#90152](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/90152)

This PR adds support for authenticating a user via the git command line via OIDC providers.

_This_ Repo supports this PR by providing a highly portable minimally sufficent environment for reproducability

Included in this repo is:

    - A docker compose environment vendoring a pre-configured OIDC provider/IDP called KeyCloak
    - Documentation detailing how to modify KeyCloak identities
    - Device Flow client script written in Go
    - Instruction for setting up the device flow client and general useage/expectations
    - Setup script for local environment

To get started:

    1. Setup the GDK and your local Gitlab development environment as you normally would
    2. Checkout [#90152](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/90152)
    3. Start up GDK
    4. Spin up docker-compose OIDC environment
    5. Run local environment setup script (note this may require you to install Go and related Go packages)
    6. Establish an account on local Gitlab instance with OIDC provider identity (this can be default or one you create)
    7. Create a private repo via Gitlab UI with aforementioned user
    8. Clone said repo locally via https. When prompted for input follow instructions in the terminal.
    9. Behvaior is verified upon sucessfull authentication and clone

Docker-Compose Instructions
----------------------------

In order to establish the KeyCloak environment and ODIC IDP you'll need to spin up the docker compose environment provided in this repo.

To do this you'll need to have installed Docker-Compose on your local system. Once that requirement has been met, certain changes will need to be made to the provided .env file.

You may want to change the ports that are allocated on the
host.  You can also override the settings by setting them as environment
variables. Further, you _MUST_ override the `EXTERNAL_GITLAB_HOST`, `HOSTNAME`, and `GITLAB_HOSTNAME` values to ensure proper routing and allow KeyCloak and Gitlab to find eachother. More instructions to that end can be found in the .env file itself.
After the proper env is established, the environment can be spun up by running the commands

    ```bash
    $ docker-compose -f build.yml build
    $ docker-compose up -d keycloak
    ```

KeyCloak Instructions
---------------------

Once the keycloak instance is running in the Docker Compose environment, browse to ``localhost:8090``, select
"Administration Console" and log in.  The default user name and password are
both ``keycloak``.  Once logged in, you should be automatically redirected to
the settings page for the default ``Gitlab`` realm.  You can confirm this by
noting the "Gitlab" name displayed in the top-left area of the page.  Click on
the "Users" link under the "Manage" section and then click the "Add user" button
to add a new user to the ``Gitlab`` realm.  When adding this user, be sure to
include an email address and toggle "Email Verified" to "ON".  Once added, click
on the "Credentials" tab to set a password for the new user.

After creating the above user in the ``Gitlab`` realm, browse back to the gitlab
site.  You should now be able to log in to gitlab through keycloak as an OpenID
connect provider.  When logging in through keycloak, use the user name and
password that you added above.
Now that you are able to log in and Gitlab is aware of your user, you've completed step 6 above and are ready to move on to futher steps.
