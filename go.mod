module main

go 1.17

require github.com/cli/oauth v0.9.0

require (
	github.com/cli/browser v1.0.0 // indirect
	github.com/cli/safeexec v1.0.0 // indirect
)
